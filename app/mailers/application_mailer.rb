class ApplicationMailer < ActionMailer::Base

  default from: 'henry@excaliburbritish.com'
  layout 'mailer'
end
