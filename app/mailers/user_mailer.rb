class UserMailer < ApplicationMailer

  def account_activation(user)
  
    @user = user
    @greeting = "Hi"
    mail to: user.email, subject: "Account activation"
  
  end

  def password_reset(user)
    
    @user = user
    if @user
       @greeting = "Hi"
       mail to: user.email, subject: "Password Reset"
    else
    end
  end
end